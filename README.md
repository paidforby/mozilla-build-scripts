# Mozilla Build Scripts

This is a repository containing scripts for building and publishing Mozilla libraries to public Maven Central repositories.

## Getting started

1. Clone this repository and all submodules.

```
git clone https://gitlab.com/paidforby/mozilla-build-scripts.git --recursive
```

2. Enter the checkout directory and manually clone the mozilla-release repo. Since it is a mercurial repo, it cannot easily be included as a submodule.
```
cd mozilla-build-scripts
hg clone https://hg.mozilla.org/releases/mozilla-release/
hg update "tag(FIREFOX_109_0_1_RELEASE)"
```

3. Enter the build directory and start the build script for the desired architectures,
```
cd build
./build.sh -r -a arm64-v8a -a armeabi-v7a -a omni
```
This example command will build geckoview-ceno artifacts for both 64 and 32 bit architectures as well as a fat-AAR that bundles both archtectures. After building, it will publish them to your local maven repository.


## Updating patches

The `gecko-liberate.patch` is maintained by relan and/or Tad for Fennec Fdroid and Mull Fenix, so that should be simply copied when it changes in upstream fennecbuild.  

The `gecko-certinject.patch` and `gecko-cenopublish.patch` are maintained by us (Ceno developers) and must be re-patched manually if a conflicting change is made in a new release of geckoview.  

To update our patches:  

1. Follow steps 1 and 2 above for cloning this repo and the mozilla-release repo, but do not start the build script.  

2. Try applying the existing patch manually,  
```
patch -p1 --no-backup-if-mismatch < "../gecko-certinject.patch"
```
If it breaks, then it should report how many hunks in which files failed. In some cases, it might not fail in an obvious way until building or (worst case scenario) running geckoview.  

3. Once a solution is found, make the required adjustments manually to the files in mozilla-release.  

4. Export the mecurial diff to the `gecko-*.patch` file,  
```
hg diff > ../gecko-certinject.patch
```
5. Commit the new patch file.

6. Revert the changes made to create the patch,  
```
hg revert --no-backup --all
```
This will allow for testing the patch when running the build script (and so the build script won't complain about the patch already being applied).  

Now, run the build script to validate that the new patch works as expected.  

diff -r 10f793276d1a mobile/android/components/geckoview/GeckoViewStartup.sys.mjs
--- a/mobile/android/components/geckoview/GeckoViewStartup.sys.mjs	Mon Apr 08 17:42:20 2024 +0000
+++ b/mobile/android/components/geckoview/GeckoViewStartup.sys.mjs	Fri Apr 19 13:59:03 2024 -0400
@@ -7,14 +7,37 @@
 
 const lazy = {};
 
+var { XPCOMUtils } = ChromeUtils.importESModule(
+  "resource://gre/modules/XPCOMUtils.sys.mjs"
+);
+
 ChromeUtils.defineESModuleGetters(lazy, {
   ActorManagerParent: "resource://gre/modules/ActorManagerParent.sys.mjs",
   EventDispatcher: "resource://gre/modules/Messaging.sys.mjs",
   PdfJs: "resource://pdf.js/PdfJs.sys.mjs",
 });
 
+
+XPCOMUtils.defineLazyServiceGetters(lazy, {
+  gCertDB: ["@mozilla.org/security/x509certdb;1", "nsIX509CertDB"],
+});
+
 const { debug, warn } = GeckoViewUtils.initLogging("Startup");
 
+XPCOMUtils.defineLazyGetter(lazy, "log", () => {
+  let { ConsoleAPI } = ChromeUtils.importESModule(
+    "resource://gre/modules/Console.sys.mjs"
+  );
+  return new ConsoleAPI({
+    prefix: "Policies.jsm",
+    // tip: set maxLogLevel to "debug" and use log.debug() to create detailed
+    // messages during development. See LOG_LEVELS in Console.jsm for details.
+    maxLogLevel: "error",
+    maxLogLevelPref: "error",
+  });
+});
+
+
 function InitLater(fn, object, name) {
   return DelayedInit.schedule(fn, object, name, 15000 /* 15s max wait */);
 }
@@ -250,6 +273,8 @@
           "GeckoView:SetDefaultPrefs",
           "GeckoView:SetLocale",
           "GeckoView:InitialForeground",
+          "GeckoView:SetRootCertificate",
+          "GeckoView:SetProxyConfig",
         ]);
 
         Services.obs.addObserver(this, "browser-idle-startup-tasks-finished");
@@ -344,6 +369,93 @@
         );
         break;
 
+      case "GeckoView:SetRootCertificate":
+        (async () => {
+          const certfilename = aData.rootCertificate;
+          if (certfilename == "") {
+            return;
+          }
+          lazy.log.debug(`Installing cert file - ${certfilename}`);
+          let certfile;
+          try {
+            certfile = Cc["@mozilla.org/file/local;1"].createInstance(
+              Ci.nsIFile
+            );
+            certfile.initWithPath(certfilename);
+          } catch (e) {
+            lazy.log.error(`Unable to init certfile - ${certfilename}: ${e}`);
+          }
+          let file;
+          try {
+            file = await File.createFromNsIFile(certfile);
+          } catch (e) {
+            lazy.log.error(`Unable to find certificate - ${certfilename}`);
+            return;
+          }
+          const reader = new FileReader();
+          reader.onloadend = function() {
+            if (reader.readyState != reader.DONE) {
+              lazy.log.error(`Unable to read certificate - ${certfile.path}`);
+              return;
+            }
+            const certFile = reader.result;
+            const certFileArray = [];
+            for (let i = 0; i < certFile.length; i++) {
+              certFileArray.push(certFile.charCodeAt(i));
+            }
+            let cert;
+            try {
+              cert = lazy.gCertDB.constructX509(certFileArray);
+            } catch (e) {
+              try {
+                // It might be PEM instead of DER.
+                cert = lazy.gCertDB.constructX509FromBase64(
+                  pemToBase64(certFile)
+                );
+              } catch (ex) {
+                lazy.log.error(
+                  `Unable to add certificate - ${certfile.path}`,
+                  ex
+                );
+              }
+            }
+            if (cert) {
+              if (
+                lazy.gCertDB.isCertTrusted(
+                  cert,
+                  Ci.nsIX509Cert.CA_CERT,
+                  Ci.nsIX509CertDB.TRUSTED_SSL
+                )
+              ) {
+                // Certificate is already installed.
+                lazy.log.debug(`Cert is already installed: ${certFile}`);
+                return;
+              }
+              try {
+                lazy.gCertDB.addCert(certFile, "CT,CT,");
+              } catch (e) {
+                // It might be PEM instead of DER.
+                lazy.gCertDB.addCertFromBase64(pemToBase64(certFile), "CT,CT,");
+              }
+            }
+          };
+          reader.readAsBinaryString(file);
+        })();
+        break;
+
+      case "GeckoView:SetProxyConfig":
+        if (aData.Mode) {
+          let defaults =Services.prefs.getDefaultBranch("");
+          defaults.setIntPref("network.proxy.type", PROXY_TYPES_MAP.get(aData.Mode))
+        }
+        if (aData.HTTPProxy) {
+          setProxyHostAndPort("http", aData.HTTPProxy);
+        }
+        if (aData.SSLProxy) {
+          setProxyHostAndPort("ssl", aData.SSLProxy);
+        }
+        break;
+
       case "GeckoView:StorageDelegate:Attached":
         InitLater(() => {
           const loginDetection = Cc[
@@ -356,6 +468,39 @@
   }
 }
 
+function pemToBase64(pem) {
+  return pem
+    .replace(/(.*)-----BEGIN CERTIFICATE-----/, "")
+    .replace(/-----END CERTIFICATE-----(.*)/, "")
+    .replace(/[\r\n]/g, "");
+}
+
+var PROXY_TYPES_MAP = new Map([
+  ["none", Ci.nsIProtocolProxyService.PROXYCONFIG_DIRECT],
+  ["system", Ci.nsIProtocolProxyService.PROXYCONFIG_SYSTEM],
+  ["manual", Ci.nsIProtocolProxyService.PROXYCONFIG_MANUAL],
+  ["autoDetect", Ci.nsIProtocolProxyService.PROXYCONFIG_WPAD],
+  ["autoConfig", Ci.nsIProtocolProxyService.PROXYCONFIG_PAC],
+]);
+
+function setProxyHostAndPort(type, address) {
+  let url
+  let defaults = Services.prefs.getDefaultBranch("")
+  try {
+    // Prepend https just so we can use the URL parser
+    // instead of parsing manually.
+    url = new URL(`https://${address}`);
+  } catch (e) {
+    lazy.log.error(`Invalid address for ${type} proxy: ${address}`);
+    return;
+  }
+
+  defaults.setStringPref(`network.proxy.${type}`, url.hostname);
+  if (url.port) {
+    defaults.setIntPref(`network.proxy.${type}_port`, Number(url.port));
+  }
+}
+
 GeckoViewStartup.prototype.classID = Components.ID(
   "{8e993c34-fdd6-432c-967e-f995d888777f}"
 );
diff -r 10f793276d1a mobile/android/geckoview/src/main/java/org/mozilla/geckoview/GeckoRuntimeSettings.java
--- a/mobile/android/geckoview/src/main/java/org/mozilla/geckoview/GeckoRuntimeSettings.java	Mon Apr 08 17:42:20 2024 +0000
+++ b/mobile/android/geckoview/src/main/java/org/mozilla/geckoview/GeckoRuntimeSettings.java	Fri Apr 19 13:59:03 2024 -0400
@@ -580,12 +580,62 @@
       getSettings().setLargeKeepaliveFactor(factor);
       return this;
     }
+
+    /**
+     * Path to a root CA certificate file which GeckoView will load into x509 certdb
+     *
+     * @param rootCertificate Path to root CA certificate file in DER or PEM format to read from or
+     *     <code>null</code> to use leave the certdb unchanged
+     * @return This Builder instance.
+     */
+    public @NonNull Builder rootCertificate(final @Nullable String rootCertificate) {
+      getSettings().mRootCertificate = rootCertificate;
+      return this;
+    }
+
+    /**
+     * Proxy type which GeckoView will set as default preference
+     *
+     * @param proxyType String containing the mode to which proxy will be set,
+     *     valid strings include ["none", "system", "manual", "autoDetect", "autoConfig"]
+     * @return This Builder instance.
+     */
+    public @NonNull Builder proxyType(final @Nullable String proxyType) {
+      getSettings().mProxyType = proxyType;
+      return this;
+    }
+
+    /**
+     * HTTP Proxy which GeckoView will set as default preference
+     *
+     * @param httpProxy String containing hostname:port for HTTP Proxy
+     * @return This Builder instance.
+     */
+    public @NonNull Builder httpProxy(final @Nullable String httpProxy) {
+      getSettings().mHttpProxy = httpProxy;
+      return this;
+    }
+
+    /**
+     * SSL Proxy which GeckoView will set as default preference
+     *
+     * @param sslProxy String containing hostname:port for HTTP Proxy
+     * @return This Builder instance.
+     */
+    public @NonNull Builder sslProxy(final @Nullable String sslProxy) {
+      getSettings().mSslProxy = sslProxy;
+      return this;
+    }
   }
 
   private GeckoRuntime mRuntime;
   /* package */ String[] mArgs;
   /* package */ Bundle mExtras;
   /* package */ String mConfigFilePath;
+  /* package */ String mRootCertificate;
+  /* package */ String mProxyType;
+  /* package */ String mHttpProxy;
+  /* package */ String mSslProxy;
 
   /* package */ ContentBlocking.Settings mContentBlocking;
 
@@ -721,11 +771,17 @@
     mConfigFilePath = settings.mConfigFilePath;
     mTelemetryProxy = settings.mTelemetryProxy;
     mExperimentDelegate = settings.mExperimentDelegate;
+    mRootCertificate = settings.mRootCertificate;
+    mProxyType = settings.mProxyType;
+    mHttpProxy = settings.mHttpProxy;
+    mSslProxy = settings.mSslProxy;
   }
 
   /* package */ void commit() {
     commitLocales();
+    commitRootCertificate();
     commitResetPrefs();
+    commitProxyConfig();
   }
 
   /**
@@ -1646,6 +1702,55 @@
     return this;
   }
 
+  /**
+   * Get path to a root CA certificate file which GeckoView will load into x509 certdb
+   *
+   * @return Path to root CA certificate file in DER or PEM format to be read from
+   */
+  public @Nullable String getRootCertificate() {
+    return mRootCertificate;
+  }
+
+  /**
+   * Set path to a root CA certificate file which GeckoView will load into x509 certdb
+   *
+   * @param rootCertificate Path to root CA certificate file in DER or PEM format to read from or
+   *     <code>null</code> to use leave the certdb unchanged
+   */
+  public void setRootCertificate(final @Nullable String rootCertificate) {
+    mRootCertificate = rootCertificate;
+    commitRootCertificate();
+  }
+
+  private void commitRootCertificate() {
+    if (mRootCertificate == "") {
+      return;
+    } else {
+      final GeckoBundle data = new GeckoBundle(1);
+      data.putString("rootCertificate", mRootCertificate);
+      EventDispatcher.getInstance().dispatch("GeckoView:SetRootCertificate", data);
+    }
+  }
+
+  public void setProxyConfig(final @Nullable String proxyType, @Nullable String httpProxy, @Nullable String sslProxy) {
+    mProxyType = proxyType;
+    mHttpProxy = httpProxy;
+    mSslProxy = sslProxy;
+    commitProxyConfig();
+  }
+
+  private void commitProxyConfig() {
+    if (mProxyType == "") {
+      return;
+    } else {
+      final GeckoBundle data = new GeckoBundle(1);
+      data.putString("Mode", mProxyType);
+      data.putString("HTTPProxy", mHttpProxy);
+      data.putString("SSLProxy", mSslProxy);
+      EventDispatcher.getInstance().dispatch("GeckoView:SetProxyConfig", data);
+    }
+  }
+
   // For internal use only
   /* protected */ @NonNull
   GeckoRuntimeSettings setProcessCount(final int processCount) {
@@ -1669,6 +1774,10 @@
     out.writeString(mCrashHandler != null ? mCrashHandler.getName() : null);
     out.writeStringArray(mRequestedLocales);
     out.writeString(mConfigFilePath);
+    out.writeString(mRootCertificate);
+    out.writeString(mProxyType);
+    out.writeString(mHttpProxy);
+    out.writeString(mSslProxy);
   }
 
   // AIDL code may call readFromParcel even though it's not part of Parcelable.
@@ -1700,6 +1809,10 @@
 
     mRequestedLocales = source.createStringArray();
     mConfigFilePath = source.readString();
+    mRootCertificate = source.readString();
+    mProxyType = source.readString();
+    mHttpProxy = source.readString();
+    mSslProxy = source.readString();
   }
 
   public static final Parcelable.Creator<GeckoRuntimeSettings> CREATOR =

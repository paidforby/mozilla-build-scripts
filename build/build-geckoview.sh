#!/bin/bash

# (m-c = mozilla-central repo)

set -e

BUILD_DIR=$(realpath $(pwd))/geckoview
mkdir -p "${BUILD_DIR}"
SOURCE_DIR=$(realpath $(pwd))

MIN_SDK=21

IS_RELEASE_BUILD=0

export MOZBUILD_STATE_PATH="${HOME}/.mozbuild"
export PATH="$HOME/.cargo/bin:$PATH"
ANDROID_SDK="${MOZBUILD_STATE_PATH}/android-sdk-linux"
ANDROID_NDK="${ANDROID_SDK}/ndk/26.2.11394342"

function usage {
    echo "build-geckoview.sh -- Builds mozilla-central binaries for android"
    echo "Usage: build-geckoview.sh [OPTION]..."
    echo "  -r                            Make a release build."
    exit 1
}

while getopts r option; do
    case "$option" in
        r)
            IS_RELEASE_BUILD=1
            shift $((OPTIND -1))
            ;;
        *)
            usage
    esac
done

ABI=${ABI:-armeabi-v7a}
case "$ABI" in
    omni)
        TARGET=aarch64-linux-android
        ;;
    armeabi-v7a)
        TARGET=arm-linux-androideabi
        ;;
    arm64-v8a)
        TARGET=aarch64-linux-android
        ;;
    x86_64)
        TARGET=x86_64-linux-android
        ;;
    x86)
        TARGET=i686-linux-android
        ;;
    *)
        echo "Unknown ABI: '$ABI', valid values are armeabi-v7a, arm64-v8a, x86 and x86_64."
        exit 1
esac
if [ $IS_RELEASE_BUILD -eq 1 ]; then
    VARIANT=release
    SUFFIX=-ceno
else
    VARIANT=debug
    SUFFIX=-default
fi

ABI_BUILD_DIR="${BUILD_DIR}"/build-${ABI}-${VARIANT}
MODES=
ALLOWED_MODES="patch build publish"
DEFAULT_MODES="patch build"

function check_mode {
    if echo "$MODES" | grep -q "\b$1\b"; then
        return 0
    fi
    return 1
}

function update_mc {
    pushd "${MOZ_DIR}" >/dev/null
    # Check if currently checked out id contains target tag
    if [[ ! $(hg id) =~ "${MOZ_FF_TAG}" ]]; then
        # If not, pull and update to the target tag
        hg pull
        hg update "tag(${MOZ_FF_TAG})"
    fi
    popd >/dev/null
}

function patch_mc {
    local COOKIE_FILE="${BUILD_DIR}"/.finished-patch
    if [[ -e "${COOKIE_FILE}" ]]; then
        return
    fi
    pushd "${MOZ_DIR}" >/dev/null

    # Patch to add feature for injecting custom CA certs
    patch -p1 --no-backup-if-mismatch --quiet < "${SOURCE_DIR}/gecko-certinject.patch"

    # Patch gradle files to allow signing and publishing to Sonatype
    patch -p1 --no-backup-if-mismatch --quiet < "${SOURCE_DIR}/gecko-cenopublish.patch"

    # Patches below taken from fennecbuild
    # Patch the use of proprietary libraries
    patch -p1 --no-backup-if-mismatch --quiet < "${SOURCE_DIR}/gecko-liberate.patch"

    # Fix v125 compile error
    patch -p1 --no-backup-if-mismatch --quiet < "${SOURCE_DIR}/gecko-fix-125-compile.patch"

    # Disable Gecko Media Plugins and casting
    sed -i -e '/gmp-provider/d; /casting.enabled/d' mobile/android/app/geckoview-prefs.js
    cat << EOF >> mobile/android/app/geckoview-prefs.js

// Disable Encrypted Media Extensions
pref("media.eme.enabled", false);

// Disable Gecko Media Plugins
pref("media.gmp-provider.enabled", false);

// Avoid openh264 being downloaded
pref("media.gmp-manager.url.override", "data:text/plain,");

// Disable openh264 if it is already downloaded
pref("media.gmp-gmpopenh264.enabled", false);

// Disable casting (Roku, Chromecast)
pref("browser.casting.enabled", false);
EOF

    sed -i -e '/check_android_tools("emulator"/d' build/moz.configure/android-sdk.configure

    popd >/dev/null
    touch "${COOKIE_FILE}"
}

function write_build_config {
    function cp_if_different {
        local from="$1"
        local to="$2"
        cmp -s "$from" "$to" || cp "$from" "$to"
    }

    mkdir -p "${ABI_BUILD_DIR}"

    pushd "${ABI_BUILD_DIR}" >/dev/null

    # Configure
    cat > mozconfig-new <<MOZCONFIG_BASE
# Copied from fennecbuild
ac_add_options --disable-crashreporter
ac_add_options --disable-debug
ac_add_options --disable-nodejs
ac_add_options --disable-tests
ac_add_options --disable-updater
ac_add_options --enable-application=mobile/android
ac_add_options --enable-release
ac_add_options --enable-minify=properties # JS minification breaks addons
ac_add_options --enable-update-channel=release
ac_add_options --target=$TARGET
ac_add_options --with-android-min-sdk=$MIN_SDK
ac_add_options --with-android-sdk="${ANDROID_SDK}"
ac_add_options --with-android-ndk="${ANDROID_NDK}"
ac_add_options --without-wasm-sandboxed-libraries
ac_add_options --with-java-bin-path="/usr/bin"
# end copied from fennec build

ac_add_options --enable-linker=lld
ac_add_options CC="${MOZBUILD_STATE_PATH}/clang/bin/clang"
ac_add_options CXX="${MOZBUILD_STATE_PATH}/clang/bin/clang++"

mk_add_options MOZ_OBJDIR="${ABI_BUILD_DIR}"

ac_add_options --disable-crashreporter
ac_add_options --disable-updater
MOZCONFIG_BASE

    # let's not worry about making debug builds right now
    #if [[ $IS_RELEASE_BUILD -eq 1 ]]; then
    #    echo "export MOZILLA_OFFICIAL=1" >> mozconfig-new
        # This disables Android Strict Mode for release builds
        # (i.e. "StrictMode policy violation" messages in the Android log),
        # according to `org.mozilla.gecko.GeckoApp.onCreate()`.
        # It also disables site issue reporting,
        # according to `gecko-dev/mobile/android/extensions/moz.build`
        # and `gecko-dev/mobile/android/locales/jar.mn`.
    #    echo "ac_add_options --enable-update-channel=release" >> mozconfig-new
        #echo "ac_add_options --enable-release" >> mozconfig-new
    #    echo "ac_add_options --disable-debug" >> mozconfig-new
    #    echo "ac_add_options --enable-optimize" >> mozconfig-new
    #else
    #    echo "ac_add_options --enable-debug" >> mozconfig-new
    #fi

    # not sure this is needed anymore, isn't in fennec build?
    #if [ "$ABI" == armeabi-v7a -o "$ABI" == x86 -o "$ABI" == x86_64 ]; then
        # See https://mozilla.logbot.info/mobile/20190706#c16442172
        # This can be removed when the bug causing it is fixed.
    #    echo "ac_add_options --disable-elf-hack" >> mozconfig-new
    #fi

    if [ "$ABI" == omni ]; then
        mkdir -p "${MOZ_FETCHES_DIR}"
        MOZ_MAJOR_VER=$(echo $MOZ_VER | cut -d. -f1)
        export MOZ_FETCHES_DIR=${MOZ_FETCHES_DIR}
        export MOZ_ANDROID_FAT_AAR_ARCHITECTURES="armeabi-v7a,arm64-v8a,x86,x86_64"
        export MOZ_ANDROID_FAT_AAR_ARM64_V8A=geckoview${SUFFIX}-omni-arm64-v8a-${MOZ_VER}.aar
        export MOZ_ANDROID_FAT_AAR_ARMEABI_V7A=geckoview${SUFFIX}-omni-armeabi-v7a-${MOZ_VER}.aar
        export MOZ_ANDROID_FAT_AAR_X86=geckoview${SUFFIX}-omni-x86-${MOZ_VER}.aar
        export MOZ_ANDROID_FAT_AAR_X86_64=geckoview${SUFFIX}-omni-x86_64-${MOZ_VER}.aar
    fi

    # Maybe required for multi-locale builds? see, https://bugzilla.mozilla.org/show_bug.cgi?id=1658040#c5
    export GRADLE_INVOKED_WITHIN_MACH_BUILD=1

    export MOZCONFIG="${ABI_BUILD_DIR}/mozconfig"
    export MOZ_BUILD_DATE=$(echo $MOZ_VER | cut -d. -f3)
    export GROUP_ID="${GROUP_ID}"
    export OSSRH_USERNAME="${OSSRH_USERNAME}"
	export OSSRH_PASSWORD="${OSSRH_PASSWORD}"
    export SIGNING_PASSWORD="${SIGNING_PASSWORD}"
    export SIGNING_KEY_ID="${SIGNING_KEY_ID}"
    export SIGNING_KEY="${SIGNING_KEY}"
    export EXOPLAYER_VERSION="${MOZ_VER}"

    export MOZILLA_OFFICIAL=1

    # This is set in fennecbuild before mach build is run
    export MACH_BUILD_PYTHON_NATIVE_PACKAGE_SOURCE=none

    echo "#define MOZ_BUILDID ${MOZ_BUILD_DATE}" > buildid.h

    cp_if_different mozconfig-new mozconfig

    popd >/dev/null
}

function build_gmscore {
    # Build microG libraries
    pushd "${HOME}/build/GmsCore" >/dev/null
    ANDROID_HOME=${ANDROID_SDK} ./gradlew -x javaDocReleaseGeneration \
        :play-services-ads-identifier:publishToMavenLocal \
        :play-services-base:publishToMavenLocal \
        :play-services-basement:publishToMavenLocal \
        :play-services-fido:publishToMavenLocal \
        :play-services-tasks:publishToMavenLocal
    popd
}

function build_geckoview_aar {
    pushd "${MOZ_DIR}" >/dev/null
    echo "---------- BUILDING GECKOVIEW AAR ----------"
    export MOZ_CHROME_MULTILOCALE=$(cat "${SOURCE_DIR}"/locales)
    ./mach build --verbose
    echo "---------- FINISHED BUILDING GECKOVIEW AAR ----------"
    popd >/dev/null
}

function publish_local_geckoview_aar {
    pushd "${MOZ_DIR}" >/dev/null
    echo "---------- PUBLISHING AAR LOCALLY ----------"
    ./mach build binaries && ./mach gradle geckoview:publishWithGeckoBinariesReleasePublicationToMavenRepository
    echo "---------- FINISHED PUBLISHING AAR LOCALLY ----------"
    popd >/dev/null
}

function publish_geckoview_aar {
    pushd "${MOZ_DIR}" >/dev/null
    echo "---------- PUBLISHING AAR TO SONATYPE ----------"
    ./mach gradle geckoview:publishWithGeckoBinariesReleasePublicationToSonatypeRepository
    echo "---------- FINISHED PUBLISHING AAR SONATYPE ----------"
    popd >/dev/null
}

######################################################################

# Parse modes and leave emulator arguments.

progname=$(basename "$0")
if [ "$1" = --help ]; then
    echo "Usage: $progname [MODE...] [-- EMULATOR_ARG...]"
    echo "Accepted values of MODE: $ALLOWED_MODES"
    echo "If no MODE is provided, assume \"$DEFAULT_MODES\"."
    exit 0
fi

while [ -n "$1" -a "$1" != -- ]; do
    if ! echo "$ALLOWED_MODES" | grep -q "\b$1\b"; then
        echo "$progname: unknown mode \"$1\"; accepted modes: $ALLOWED_MODES" >&2
        exit 1
    fi
    MODES="$MODES $1"
    shift
done

if [ "$1" = -- ]; then
    shift  # leave the rest of arguments for the emulator
fi

if [ ! "$MODES" ]; then
    MODES="$DEFAULT_MODES"
fi

if check_mode patch; then
    update_mc
    patch_mc
fi

if check_mode build; then
    write_build_config
    build_gmscore
    build_geckoview_aar
    publish_local_geckoview_aar
fi

if check_mode publish; then
    if [  $IS_RELEASE_BUILD -eq 0 ]; then
        echo "Artifacts can be published only when the build is a Release."
        exit 1;
    else
        publish_geckoview_aar
    fi
fi
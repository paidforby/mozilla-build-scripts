#!/bin/bash

set -e

BUILD_DIR=$(pwd)/geckoview
mkdir -p "${BUILD_DIR}"
SOURCE_DIR=$(pwd)
GECKO_DIR=$(pwd)/../mozilla-release
MOZ_FETCHES_DIR=${BUILD_DIR}/fetches

SUPPORTED_ABIS=(armeabi-v7a arm64-v8a omni x86 x86_64)
RELEASE_DEFAULT_ABIS=(armeabi-v7a arm64-v8a x86 x86_64 omni)
DEFAULT_ABI=arm64-v8a
RELEASE_KEYSTORE_KEY_ALIAS=upload

CLEAN=false
BUILD_RELEASE=false
PUBLISH_RELEASE=false
BUILD_DEBUG=false
VARIANT=
GECKO_VARIANT_FLAGS=

ABIS=()

function usage {
    echo "build-dependencies.sh -- Builds Android Archive dependencies required for CENO Browser"
    echo "Usage: build-dependencies.sh [OPTION]..."
    echo "  -c                            Remove build files (keep downloaded dependencies)"
    echo "  -r                            Build a release build"
    echo "  -d                            Build a debug build. Will optionally apply -x and -v. This is the default."
    echo "  -p                            Publish release AARs to Sonatype, credentials and keys must be"
    echo "                                provided as enviroment variables"
    echo "  -a <abi>                      Build for android ABI <abi>. Can be specified multiple times."
    echo "                                Supported ABIs are [${SUPPORTED_ABIS[@]}]."
    echo "                                Default for debug builds is ${DEFAULT_ABI}."
    echo "                                Default for release builds is ${RELEASE_DEFAULT_ABIS}."
    echo "  -g <gecko-dir>                The directory where local copy of gecko-dev source code is stored"
    exit 1
}

while getopts crda:g:p option; do
    case "$option" in
        c)
            CLEAN=true
            ;;
        r)
            BUILD_RELEASE=true
            ;;
        d)
            BUILD_DEBUG=true
            ;;
        a)
            supported=false
            for i in ${SUPPORTED_ABIS[@]}; do [[ $i = $OPTARG ]] && supported=true && break; done
            listed=false
            for i in ${ABIS[@]}; do [[ $i = $OPTARG ]] && listed=true && break; done

            if ! $supported; then
                echo "Unknown ABI. Supported ABIs are [${SUPPORTED_ABIS[@]}]."
                exit 1
            fi
            if ! $listed; then
                ABIS+=($OPTARG)
            fi
            ;;
        g)
            GECKO_DIR="${OPTARG}"
            ;;
        p)
            BUILD_RELEASE=true
            PUBLISH_RELEASE=true
            ;;
        *)
            usage
    esac
done

function get_set_abis {
    if [[ ${#ABIS[@]} -eq 0 ]]; then
        if $BUILD_RELEASE; then
            ABIS=${RELEASE_DEFAULT_ABIS[@]}
        else
            ABIS=($DEFAULT_ABI)
        fi
    fi
}

function check_variant {
    for variant in debug release; do
        if [[ $variant = debug ]]; then
            $BUILD_DEBUG || continue
            VARIANT=debug
            GECKO_VARIANT_FLAGS="patch build"
            SUFFIX=-default
        else
            $BUILD_RELEASE || continue
            VARIANT=release
            if $PUBLISH_RELEASE; then
                GECKO_VARIANT_FLAGS="-r patch build publish"
            else
                GECKO_VARIANT_FLAGS="-r patch build"
            fi
            SUFFIX=-ceno
        fi
    done
}

function build_geckoview {
    for ABI in ${ABIS[@]}; do
        ABI_BUILD_DIR="${BUILD_DIR}"/build-${ABI}-${VARIANT}

        ABI=${ABI} \
        MOZ_DIR=${GECKO_DIR} \
        MOZ_VER=${GECKOVIEW_VERSION} \
        MOZ_FETCHES_DIR=${MOZ_FETCHES_DIR} \
        MOZ_FF_TAG=${FF_TAG} \
        GROUP_ID=${GROUP_ID:-"ie.equalit.ouinet"} \
        OSSRH_USERNAME=${OSSRH_USERNAME:-"username"} \
        OSSRH_PASSWORD=${OSSRH_PASSWORD:-"password"} \
        SIGNING_PASSWORD=${SIGNING_PASSWORD:-"signing_pass"} \
        SIGNING_KEY_ID=${SIGNING_KEY_ID:-C4FEC0D3} \
        SIGNING_KEY=${SIGNING_KEY:-"-----BEGIN PGP PRIVATE KEY BLOCK-----"} \
        ./build-geckoview.sh ${GECKO_VARIANT_FLAGS}

        if [ "${ABI}" != omni ]; then
            AAR_OUTPUT_DIR="${ABI_BUILD_DIR}"/gradle/maven/${GROUP_ID//.//}/geckoview${SUFFIX}-omni-${ABI}/${GECKOVIEW_VERSION}
            mkdir -p "${MOZ_FETCHES_DIR}" && cp -f "${AAR_OUTPUT_DIR}"/*.aar ${MOZ_FETCHES_DIR}/.
        fi
    done
}

get_set_abis
check_variant
build_geckoview
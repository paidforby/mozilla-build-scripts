#!/bin/bash
FF_REPO=firefox-android
AC_RELESE_PATTERN="components-v...\..$\|components-v...\..\..$"
GECKO_FILE="$FF_REPO/android-components/plugins/dependencies/src/main/java/Gecko.kt"
FF_TAGS_URL="https://hg.mozilla.org/releases/mozilla-release/raw-tags"
FF_RELEASE_PATTERN="FIREFOX_..._._RELEASE\|FIREFOX_..._._._RELEASE"
mv versions.txt versions-old.txt
git -C ${FF_REPO} fetch
git -C ${FF_REPO} tag | grep ${AC_RELESE_PATTERN} > versions.txt
if [ $(diff versions.txt versions-old.txt) ]; then
    echo "New version of Android-Components has been released."
    AC_VERSION=$(diff versions.txt versions-old.txt | sed -n 2p | cut -d" " -f2)
    git -C ${FF_REPO} checkout ${AC_VERSION}
    GECKOVIEW_VERSION=$(grep "const val version =" < ${GECKO_FILE} | cut -d\" -f2)
    FF_TAG=$(curl ${FF_TAGS_URL} | grep ${FF_RELEASE_PATTERN} | head -n1 | cut -f1)
    echo "Building GeckView version ${GECKOVIEW_VERSION} from mozilla-release version ${FF_TAG}."
    docker run \
        --rm \
        --user geckoview \
        --network host \
        -e GROUP_ID="ie.equalit.ouinet" \
        -e GECKOVIEW_VERSION="$GECKOVIEW_VERSION" \
        -e FF_TAG="$FF_TAG" \
        -e OSSRH_USERNAME="$OSSRH_USERNAME" \
        -e OSSRH_PASSWORD="$OSSRH_PASSWORD" \
        -e SIGNING_KEY_ID="$SIGNING_KEY_ID" \
        -e SIGNING_PASSWORD="$SIGNING_PASSWORD" \
        -e SIGNING_KEY="$SIGNING_KEY" \
        cenodev/geckoview:latest
fi

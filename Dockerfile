FROM debian:bullseye as base
ENV LANG=C.UTF-8

RUN \
  apt update && apt install -y \
    autoconf \
    automake \
    autopoint \
    build-essential \
    bzip2 \
    ca-certificates-java \
    curl \
    cmake \
    gettext \
    git \
    gyp \
    g++ \
    lib32gcc-s1 \
    libffi-dev \
    libssl-dev \
    libsqlite3-dev \
    libtool \
    libz-dev \
    llvm-11 \
    m4 \
    make \ 
    mercurial \
    nasm \
    ninja-build \
    pkg-config \
    python3-distutils \
    python3-pip \
    python3-venv \
    rsync \
    software-properties-common \
    sudo \
    tcl \
    texinfo \
    unzip \
    wget \
    zlib1g-dev

RUN \
  apt-add-repository 'deb http://ftp.us.debian.org/debian bullseye-backports main' && \
  apt update && apt install -y sdkmanager

RUN \
  apt-add-repository 'deb http://security.debian.org/debian-security bullseye-security main' && \
  apt update && apt install -y openjdk-17-jdk-headless

# quieten wget and unzip
RUN echo 'quiet = on' >> /etc/wgetrc

RUN \
  echo "[extensions]" >> /etc/mercurial/hgrc && \
  echo "purge =" >> /etc/mercurial/hgrc

RUN \
  adduser --disabled-password --gecos "" geckoview && \
  echo "geckoview ALL=(ALL) NOPASSWD:ALL" >> /etc/sudoers

FROM base as bootstrap
USER geckoview

ENV USER_HOME /home/geckoview
ENV BUILD_DIR $USER_HOME/build
ENV PATH=$PATH:$USER_HOME/gradle/gradle-7.6/bin:$USER_HOME/.cargo/bin
ENV MOZ_DIR=$USER_HOME/mozilla-release
ENV SDK_DIR=${USER_HOME}/.mozbuild/android-sdk-linux

WORKDIR $USER_HOME

COPY --chown=geckoview ./mozilla-release/ $MOZ_DIR
COPY --chown=geckoview ./bootstrap.sh $USER_HOME/bootstrap.sh
RUN ./bootstrap.sh

FROM bootstrap

USER geckoview

WORKDIR $BUILD_DIR
COPY --chown=geckoview ./build/ $BUILD_DIR/
RUN git clone -b v0.3.0.233515 https://github.com/microg/GmsCore.git

ENTRYPOINT ./build.sh -p

#!/bin/bash

# Install rust https://www.rust-lang.org/en-US/install.html
curl https://sh.rustup.rs -sSf | sh -s -- -y
rustup update
rustup toolchain install 1.77.2
rustup default 1.77.2
rustup target add thumbv7neon-linux-androideabi
rustup target add armv7-linux-androideabi
rustup target add aarch64-linux-android
rustup target add x86_64-linux-android
rustup target add i686-linux-android
cargo install --force --vers 0.26.0 cbindgen

pushd "${MOZ_DIR}" >/dev/null
./mach --no-interactive bootstrap --application-choice="GeckoView/Firefox for Android"
popd  >/dev/null

pushd "${SDK_DIR}/cmdline-tools/12.0/bin" >/dev/null
yes y | ./sdkmanager --install "ndk;25.2.9519653"
yes y | ./sdkmanager --install "ndk;26.2.11394342"
popd  >/dev/null
